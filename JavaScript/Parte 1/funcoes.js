//estudo de funçoes / nomear funçoes = verbo + substantivo
//para declarar uma funçao usa-se FUNCTION + NOME parenteses e chaves
let corSite = "azul";
//abaixo, funçao qua reseta cor da variavel para vazio. Neste primeiro caso a funçao é sem parametros
function resetarCor (){
    corSite = "";
};

//valor atual da variavel
console.log('valor primario atribuido: ' + corSite);
resetarCor ();
console.log('o valor atual da cor é: ' + corSite);

//agora vamos incrementar a funçao com uso de parametros
function resetaCor2 (cor){
    corSite = cor;
}
resetaCor2('vermelho'); //passando o parametro cor
console.log(corSite);
//O parametro pode ser de qualquer tipo: string, number, boole, etc

//funçao nova acrescentando mais um parametro a funcao resetaCor2
function resentaCor3 (cor, tonalidade){
    corSite = cor + tonalidade;
}
//atribuindo os dois parametros e exibindo em seguida
resentaCor3('verde', ' escuro');
console.log(corSite);