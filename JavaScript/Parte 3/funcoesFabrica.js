//vamos criar funçoes para receber dados de objetos sem ter que replicar esse objeto centenas de vezes, por ex
const celular = {
    marca: 'sansung',
    tamanhoTela: {
        vertical: 155,
        horizontal: 75
    },
    capacidadeBateria: 5000,
    ligar: function(){
        console.log('fazendo ligação...');
    }
}
//usando uma funçao de fabrica (factory functions):
/*basicamente vamos criar uma funçao construtora e jogar o conteudo do objeto dentro dessa function
abaixo teremos a primeira forma de fazer isso: pegando o objeto criado e jogando dentro da funçao*/
function criarCelular (){
    const celular = {
        marca: 'sansung',
        tamanhoTela: {
            vertical: 155,
            horizontal: 75
        },
        capacidadeBateria: 5000,
        ligar: function(){
            console.log('fazendo ligação...');
        }
    }
    return celular;   
}
/*a segunda forma de fazer isso é usando a função com parametros e ao inves de declarar o objeto retornando algo
ao final, usamos o return no inicio do objeto. Veja: */
