//vamos fazer um programa que troque variaveis
let a = 10;
let b = 20;
console.log('O valor de a é: ',a);
console.log('O valor de b é: ',b);
//trocando os valores
let troca = b;
b = a;
a = troca;
console.log("Apresentando os valores trocados:");
console.log('O valor de a é: ',a);
console.log('O valor de b é: ',b);