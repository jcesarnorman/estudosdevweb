//Operadores aritimeticos (matematicos +[soma], -[subtraçao], *[multiplicaçao], /[divisao] e **[potencia])
//Operadores de atribuição
//Operadores de comparação
//Operadores lógicos
//Operadores bitwase

//incremento e decremento ++ --
//incremento pós e pre ++
console.log('INCREMENTO E DECREMENTO');
let valor = 10;
console.log(valor++); //aqui primeiro ele exibe o valor atual (10), depois incrementa de 1
console.log(valor); //aqui o valor ja foi incrementado de 1

let valor2 = 5;
console.log(++valor2); //aqui, antes de exibir o valor é incrementado de 1

//para o decremento, funciona da mesma forma

//operador de potencia
console.log('USO DE POTENCIA');
let pot = 2**4;
console.log(pot);

//operadores de atribuiçao. O = ja esta sendo usado desde o começo
console.log('OPERADORES DE ATRIBUIÇAO');
//vamnos usar suas variaçoes
let valorItem = 100;
console.log(valorItem);
//uso do +=
valorItem += valorItem; //é o mesmo que jogar dentro de valorItem a soma de valorItem com ele mesmo:
//valorItem = valorItem + valorItem
//exibindo agora temos o resultado acima
console.log(valorItem);
//funciona no inverso para o -=