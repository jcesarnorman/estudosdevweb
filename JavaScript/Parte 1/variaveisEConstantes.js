console.log("Polícia Penal do RN");
//abaixo teremos a declaração e exibiçao de 3 variaveis
let nome = 'julio cesar';
let idade = 44;
let atuacao = 'desenvolvedor (DTI)';

//exibindo conteudo das variaveis concatenando com string
console.log('nome do operador: '+nome);
console.log('idade do operador: '+idade);
console.log('área de atuação: '+atuacao);

//uso de constantes
const i = 2; //esse valor nao vai se alterar ao longo do tempo
let nota1 = 9.5;
let nota2 = 8.7;
let media = (nota1 + nota2)/i;
//apresentando a media que foi obtida com o uso de variaveis e uma constante
console.log('A média do operador foi: '+media);