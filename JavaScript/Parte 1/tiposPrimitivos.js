let nome = 'Julio Cesar (Fahul)'; // string literal
let idade = 44; // number literal
let selecionado = true; // boolean
let sobrenome; //valor indefinido (tambem se pode usar o parametro UNDEFINED)
let corDaPagina = null // caso se queira redefinir um valor

//tipagem dinamica. No js pode-se mudar o tipo da variavel a qualquer tempo sem declarar o tipo, apenas mudando o valor
//com a variavel abaixo, mude os valores de atribuiçao e teste no console do browser com a funçao typeof
let teste = 14;
console.log(teste);

//noçoes de objetos: junta informaçoes em um lugar só. Abaixo temos a criaçao do objeto pessoa
//MUITO parecido com arrays
let pessoa = {
    nome: 'Julio Cesar',
    idade: 44,
    selecionado: true,
    sobrenome: 'Damasceno de Souza',
    areaAtuacao: 'TI (DTI)',
    cargo: 'Policial Penal'
};
console.log(pessoa); //aqui eu apresentei todos os elementos do objeto pessoa
console.log(pessoa.cargo); //aqui eu apresentei so um elemento do objeto pessoa