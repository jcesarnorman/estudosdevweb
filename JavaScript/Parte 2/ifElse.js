//estruturas condicionais if..else e switch..case
//usando o if..else

//se a hora for entre 6 e meio dia : bom dia
//se a hora for entre meio dia e 18: boa tarde
//caso contrario: boa noite
console.log('Uso do IF..Else');
let hora = 17;
if (hora >= 6 && hora <= 12){
    console.log('Bom dia!!!');
}else{
    if(hora <= 18){
        console.log('Boa tarde!!!');
    }else
        console.log('Boa noite!!!');
}

//usando o switch..case
//vamos atribuir um apelido aos 4 grandes times do rio
console.log('');
console.log('USO DO CASE PARA TIMES DO RIO');
let time = 'botafogo';
switch (time){
    case 'botafogo':
        console.log('O Glorioso!!');
        break;
    case 'fluminense':
        console.log('Pó de Arroz');
        break;
    case 'flamengo':
        console.log('Urubú');
        break;
    case 'vasco':
        console.log('Gigante da Colina');
        break;
    default:
        console.log('Algum time pequeno!!!');
}