function cabecalho (){
    console.log("---------------------------------------------");
    console.log("CALCULADORA SIMPLES - Vai fazer uma operação entre dois valores");
    console.log("---------------------------------------------");
}
cabecalho();
//variaveis para selecionar a operaçao
let val1 = 40;
let val2 = 20;
let operacao = 'soma';
//funcao para somar valores
function somarValores (){
    resultado = val1 + val2;
    console.log('A operaçao de soma deu: ',resultado);
};
//funcao para subtrair valores
function subtrairValores (){
    resultado = val1 - val2;
    console.log('A operaçao de subtraçao deu: ',resultado);
};
//funcao para multiplicar valores
function multiplicarValores (){
    resultado = val1 * val2;
    console.log('A operaçao de multiplicaçao deu: ',resultado);
};
//funcao para dividir valores
function dividirValores (){
    resultado = val1 / val2;
    console.log('A operaçao de divisao deu: ',resultado);
};
//agora entra o seletor que faz toda a mágica
switch (operacao){
    case 'soma':
        somarValores();
        break;
    case 'sub':
        subtrairValores();
        break;
    case 'mult':
        multiplicarValores();
        break;
    case 'div':
        dividirValores();
        break;
}