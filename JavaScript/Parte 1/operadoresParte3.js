// Falsy: valores que sao undefined, no, 0, false, '', NaN
// Truthy: nenhum dos valores acima

let corPersonalizada = 10;
let corPadrao = 'azul';
let corPerfil = corPersonalizada && corPadrao;
console.log(corPerfil);