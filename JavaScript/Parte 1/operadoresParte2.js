//operadores de igualdade
//igualdade estrita (FORMA RECOMENDADA)
console.log(1 === 1); //compara os valores e tambem os tipos
console.log('1' === 1); //vai dar falso porque os tipos sao diferentes

//igualdade solta
console.log(1 == 1); //ignora o tipo e compara apenas os valores. Veja na proxima linha que o resultado tb é true
console.log('1' == 1); //na verdade ele converte para que os tipos fiquem iguais. NÃO É RECOMENDADO!!

//Operador ternario
//cliente maior que 100 = premium, se nao comum.
let pontos = 100;
let tipo = pontos > 100 ? 'premium' : 'comum'; //usou-se aqui um if diferente do normal para testar a condiçao
console.log(tipo);

//operadores logicos: and(&&), or, not
//operador and(&&): retorna true se os dois operandos forem true
//VAMOS CONSTURUIR A TABELA VERDADE DO AND
console.log('VERD e VERD ',true && true);
console.log('VERD e FALS ',true && false);
console.log('FALS e VERD ',false && true);
console.log('FALS e FALS ',false && false);

//TABELA VERDADE DO OR (||). se um dos operadores for true o resultado é true
console.log('TABELA VERDADE DO OR');
console.log('VERD e VERD ',true || true);
console.log('VERD e FALS ',true || false);
console.log('FALS e VERD ',false || true);
console.log('FALS e FALS ',false || false);

//Operador not (!)
console.log('OPERADOR NOT');
let user = true;
console.log(user);
console.log(!user); //aqui se inverte o valor boolean