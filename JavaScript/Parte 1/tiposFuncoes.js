//DOIS TIPOS DE FUNÇAO
//primeira: realiza uma tarefa e nao devolve nada
function dizerNome (){
    console.log('Júlio Cesar');
}
dizerNome();

//segunda: realiza uma tarefa e devolve algo para uso
function multiplicarValorPorDois (valor){
    return valor*2;
}
//agora vou passar um parametro (valor) e a funçao vai multiplicar esse valor por 2
//console.log(multiplicarValorPorDois(5));
//a diferença é que eu posso atribuir a uma variavel ao resultado dessa funcao. Veja:
let resultado = multiplicarValorPorDois(10);
console.log(resultado);