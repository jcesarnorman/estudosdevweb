//Arrays: conjunto de dados acessado por um indice (posiçao da informaçao). Vamos criar um abaixo
let funcionarios = ['julio cesar', 44, true]; //array com 3 elementos de tipos diferentes
console.log(funcionarios); // exibindo todos os elementos
console.log(funcionarios[1]); // exibindo o segundo elemento (idade = 44)
console.log('tamanho: ' + funcionarios.length); // funçao para exibir o tamanho do array

//exercicio: criar um array com caracteristicas de uma pessoa conhecida
let esposa = ['sandra', 49, 'psicologa'];
//exibir linha a linha (com o uso de um label) as caracteristicas contidas no array esposa
console.log('');
console.log('DADOS DE ESPOSA');
console.log('nome: ' + esposa[0]);
console.log('idade: ' + esposa[1]);
console.log('profissao: ' + esposa[2]);